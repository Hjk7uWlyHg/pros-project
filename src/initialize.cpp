#include "main.h"

okapi::Controller masterController(okapi::ControllerId::master);
static bool autonSelected;

void updateScreen() {
  pros::lcd::set_text(0, "Auton " + std::to_string(getAutonSelection() + 1));
  pros::lcd::set_text(2, getAutonDescription());
  masterController.clear();
  pros::delay(50);
  masterController.setText(0, 0, getAutonDescription());
}

void onLeftButton() {
  if (getAutonSelection() == 0) {
    setAutonSelection(getNumAutons() - 1);
  } else {
    setAutonSelection(getAutonSelection() - 1);
  }
  updateScreen();
}

void onCenterButton() {
  pros::lcd::shutdown();
  masterController.clear();
  autonSelected = true;
}

void onRightButton() {
  if (getAutonSelection() == getNumAutons() - 1) {
    setAutonSelection(0);
  } else {
    setAutonSelection(getAutonSelection() + 1);
  }
  updateScreen();
}

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
  autonsInit();

  vis.set_wifi_mode(0);
  vis.set_signature(SIG_BLUE.id, &SIG_BLUE);
  vis.set_signature(SIG_RED.id, &SIG_RED);

  autonSelected = false;
  okapi::ControllerButton left = masterController[okapi::ControllerDigital::left];
  okapi::ControllerButton right = masterController[okapi::ControllerDigital::right];
  okapi::ControllerButton select = masterController[okapi::ControllerDigital::A];
  okapi::ControllerButton cancel = masterController[okapi::ControllerDigital::B];

  pros::lcd::initialize();
  updateScreen();
  pros::lcd::register_btn0_cb(onLeftButton);
  pros::lcd::register_btn1_cb(onCenterButton);
  pros::lcd::register_btn2_cb(onRightButton);

  while (!autonSelected) {
    if (cancel.changedToPressed() || pros::competition::is_connected()) {
      onCenterButton();
    }
    if (select.changedToPressed()) {
      startAuton();
      onCenterButton();
    }
    if (left.changedToPressed()) {
      onLeftButton();
    }
    if (right.changedToPressed()) {
      onRightButton();
    }
    pros::delay(50);
  }
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
void competition_initialize() {
  autonSelected = false;
  okapi::ControllerButton left = masterController[okapi::ControllerDigital::left];
  okapi::ControllerButton right = masterController[okapi::ControllerDigital::right];
  okapi::ControllerButton select = masterController[okapi::ControllerDigital::A];
  okapi::ControllerButton cancel = masterController[okapi::ControllerDigital::B];

  pros::lcd::initialize();
  updateScreen();
  pros::lcd::register_btn0_cb(onLeftButton);
  pros::lcd::register_btn1_cb(onCenterButton);
  pros::lcd::register_btn2_cb(onRightButton);

  while (!autonSelected) {
    if (cancel.changedToPressed() || pros::competition::is_connected()) {
      onCenterButton();
    }
    if (select.changedToPressed()) {
      startAuton();
      onCenterButton();
    }
    if (left.changedToPressed()) {
      onLeftButton();
    }
    if (right.changedToPressed()) {
      onRightButton();
    }
    pros::delay(50);
  }
}
