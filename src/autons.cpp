#include "main.h"
#include "autons.h"

static const int numAutons = 9;

static Auton autons[numAutons];

/*
 * For some reason, the robot only turns 5/6ths of what it should.
 */
#define SCALED(t) (t * 1.1 * okapi::degree)

void auton1(bool reversed = false) {
  leftGroup.tarePosition();
  rightGroup.tarePosition();
  auto chassis = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    okapi::IterativePosPIDController::Gains{0.002,0,0},
    okapi::IterativePosPIDController::Gains{0.001,0,0},
    okapi::IterativePosPIDController::Gains{0.0058,0.005,0.000125},
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  chassis.setTurnsMirrored(reversed);

  //Get ball under cap
  rollerIntakeMtr.moveRelative(400, 200);
  chassis.moveDistanceAsync(4*okapi::foot-2*okapi::inch);
  for (size_t i = 0; i < 75; i++) {
    chassis.setMaxVelocity(200.0*i/75);
    pros::delay(10);
  }
  chassis.waitUntilSettled();
  rollerIntakeMtr.moveRelative(400, 200);
  pros::delay(250);
  rollerIntakeMtr.moveVelocity(0);

  //Activate flywheel, move back, turn towards flags
  flywheelMtr.moveVelocity(-200);
  chassis.moveDistanceAsync(-4*okapi::foot + 6.5*okapi::inch);
  for (size_t i = 0; i < 75; i++) {
    chassis.setMaxVelocity(200.0*i/75);
    pros::delay(10);
  }
  chassis.waitUntilSettled();
  if (!reversed) {
    chassis.turnAngle(89.5*okapi::degree);
  } else {
    chassis.turnAngle(90*okapi::degree);
  }

  //Move, shoot top flag, move, shoot middle flag
  chassis.moveDistanceAsync(4.5*okapi::inch);
  for (size_t i = 0; i < 25; i++) {
    chassis.setMaxVelocity(8*i);
    pros::delay(10);
  }
  chassis.waitUntilSettled();
  rollerIntakeMtr.moveRelative(400, 200);
  pros::delay(250);
  chassis.moveDistanceAsync(2*okapi::foot);
  for (size_t i = 0; i < 15; i++) {
    chassis.setMaxVelocity(10*i);
    pros::delay(10);
  }
  chassis.waitUntilSettled();
  rollerIntakeMtr.moveVelocity(200);
  pros::delay(1000);
  flywheelMtr.moveVelocity(0);
  rollerIntakeMtr.moveVelocity(0);

  //Get ball under cap, then move slightly back
  chassis.setMaxVelocity(200);
  chassis.turnAngle(SCALED(15));
  chassis.moveDistance(2*okapi::foot);
  chassis.turnAngle(SCALED(-15));
  chassis.moveDistance(-2*okapi::foot);
  capFlipperMtr.moveRelative(700, 100);
  chassis.turnAngle(SCALED(-90));
  chassis.moveDistanceAsync(1.2*okapi::foot);
  pros::delay(200);
  capFlipperMtr.moveRelative(-500, 100);
  chassis.waitUntilSettled();
}

void auton2(bool reversed = false) {
  auto chassis = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  chassis.setTurnsMirrored(reversed);
  auto profileController = okapi::AsyncControllerFactory::motionProfile(
    2.0,
    2.0,
    10.0,
    chassis
  );
  auto chassisTurn = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    okapi::IterativePosPIDController::Gains{0.0025,0,0},
    okapi::IterativePosPIDController::Gains{0.005,0.001,0},
    okapi::IterativePosPIDController::Gains{0.002,0,0},
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  chassisTurn.setTurnsMirrored(reversed);
  auto flywheelController = okapi::AsyncControllerFactory::velIntegrated(flywheelMtr);

  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{4*okapi::foot - 2*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "cap"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{1*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "1in"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{1*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "1ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{2*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "2.5ft"
  );

  //Get ball under cap, then move slightly back
  rollerIntakeMtr.moveVelocity(200);
  profileController.setTarget("cap");
  profileController.waitUntilSettled();
  rollerIntakeMtr.moveVelocity(0);
  profileController.setTarget("1in", true);
  profileController.waitUntilSettled();

  //Turn towards flags on opposite side
  if (!reversed) {
    chassisTurn.turnAngle(SCALED(72.5));
  } else {
    chassisTurn.turnAngle(74*okapi::degree);
  }
  flywheelController.setTarget(-150);

  //Shoot top flag at 155 RPM and middle flag at 140 RPM
  while (std::fabs(flywheelMtr.getActualVelocity() + 155) > 2) { pros::delay(10); }
  rollerIntakeMtr.moveRelative(200, 200);
  pros::delay(250);
  flywheelController.setTarget(-140);
  pros::delay(1500);
  rollerIntakeMtr.moveVelocity(200);
  pros::delay(1000);
  rollerIntakeMtr.moveVelocity(0);

  //Turn away from platform, go slightly back, turn away from platform, get on platform
  chassisTurn.turnAngle(SCALED(-67.5));
  profileController.setTarget("1in", true);
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(-91));
  profileController.setTarget("1ft", true);
  profileController.waitUntilSettled();
  profileController.setTarget("2.5ft", true);
  profileController.waitUntilSettled();
}

void auton3(bool reversed = false) {
  leftGroup.tarePosition();
  rightGroup.tarePosition();
  auto chassis = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    okapi::IterativePosPIDController::Gains{0.002,0,0},
    okapi::IterativePosPIDController::Gains{0.001,0,0},
    okapi::IterativePosPIDController::Gains{0.0058,0.005,0.000125},
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  chassis.setTurnsMirrored(reversed);
  chassis.setMaxVelocity(100);

  chassis.moveDistance(6*okapi::foot);
}

void auton4(bool reversed = false) {
  auto chassis = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  chassis.setTurnsMirrored(reversed);
  auto profileController = okapi::AsyncControllerFactory::motionProfile(
    2.0,
    2.0,
    10.0,
    chassis
  );
  auto chassisTurn = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    okapi::IterativePosPIDController::Gains{0.0025,0,0},
    okapi::IterativePosPIDController::Gains{0.005,0.001,0},
    okapi::IterativePosPIDController::Gains{0.002,0,0},
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  chassisTurn.setTurnsMirrored(reversed);

  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{4*okapi::foot - 2*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "cap"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{1*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "1in"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{6*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "6in"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{1*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "1ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{2*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "2ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{6*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "6ft"
  );

  //Get ball under cap, then move slightly back
  rollerIntakeMtr.moveVelocity(200);
  profileController.setTarget("cap");
  profileController.waitUntilSettled();
  rollerIntakeMtr.moveVelocity(0);
  profileController.setTarget("1in", true);
  profileController.waitUntilSettled();

  //Turn towards cap, move forward and flip cap
  chassisTurn.turnAngle(SCALED(-90));
  capFlipperMtr.moveRelative(500, 200);
  profileController.setTarget("1ft");
  profileController.waitUntilSettled();
  capFlipperMtr.moveRelative(-500, 200);

  //Turn at an angle, move back to hit wall, move forward, turn towards front
  chassisTurn.turnAngle(SCALED(75));
  profileController.setTarget("6ft", true);
  profileController.waitUntilSettled();
  profileController.setTarget("6in");
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(90));

  //Move forward and release balls from roller intake
  profileController.setTarget("1ft");
  rollerIntakeMtr.moveVelocity(-200);
  profileController.waitUntilSettled();
  rollerIntakeMtr.moveVelocity(0);

  //Turn, get on platform
  chassisTurn.turnAngle(SCALED(108));
  profileController.setTarget("6ft", true);
  profileController.waitUntilSettled();
}

void autonSkill() {
  auto chassis = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  auto profileController = okapi::AsyncControllerFactory::motionProfile(
    2.0,
    2.0,
    10.0,
    chassis
  );
  auto chassisTurn = okapi::ChassisControllerFactory::create(
    leftGroup, rightGroup,
    okapi::IterativePosPIDController::Gains{0.002,0,0},
    okapi::IterativePosPIDController::Gains{0.001,0,0},
    okapi::IterativePosPIDController::Gains{0.0058,0.005,0.000125},
    CHASSIS_GEARSET,
    {WHEEL_DIAMETER, CHASSIS_WIDTH}
  );
  auto rollerIntakeController = okapi::AsyncControllerFactory::posIntegrated(rollerIntakeMtr);

  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{0.5*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "0.5ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{1*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "1ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{2*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "2ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{3*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "3ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{4*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "4ft"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{4*okapi::foot - 2*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "ball"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{4*okapi::foot - 6.5*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "alignWithFlags"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{1.5*okapi::foot, 0*okapi::foot, 0*okapi::degree}},
    "bottomFlag"
  );
  profileController.generatePath({
    okapi::Point{0*okapi::foot, 0*okapi::foot, 0*okapi::degree},
    okapi::Point{4*okapi::foot + 5*okapi::inch, 0*okapi::foot, 0*okapi::degree}},
    "alignWithPlatform"
  );

  //Move forward to get ball under cap and flip cap, then go back
  capFlipperMtr.moveRelative(275, 200);
  rollerIntakeMtr.moveRelative(100, 200);
  profileController.setTarget("ball");
  profileController.waitUntilSettled();
  capFlipperMtr.moveRelative(-275, 200);
  rollerIntakeMtr.tarePosition();
  rollerIntakeController.setTarget(600);
  rollerIntakeController.waitUntilSettled();
  flywheelMtr.moveVelocity(-200);
  profileController.setTarget("alignWithFlags", true);
  profileController.waitUntilSettled();
  rollerIntakeController.setTarget(500);
  rollerIntakeController.waitUntilSettled();

  //Turn, move to shoot top flag, then middle flag, then hit bottom flag
  chassisTurn.turnAngle(-90*okapi::degree);
  profileController.setTarget("0.5ft");
  profileController.waitUntilSettled();
  rollerIntakeMtr.tarePosition();
  rollerIntakeController.setTarget(200);
  rollerIntakeController.waitUntilSettled();
  profileController.setTarget("2ft");
  profileController.waitUntilSettled();
  rollerIntakeMtr.tarePosition();
  rollerIntakeController.setTarget(400);
  rollerIntakeController.waitUntilSettled();
  chassisTurn.turnAngle(-21*okapi::degree);
  profileController.setTarget("3ft");
  profileController.waitUntilSettled();

  //Go back, turn towards cap, move forward to flip cap
  profileController.setTarget("2ft", true);
  profileController.waitUntilSettled();
  capFlipperMtr.moveRelative(500, 200);
  chassisTurn.turnAngle(90*okapi::degree);
  profileController.setTarget("1ft");
  profileController.waitUntilSettled();
  capFlipperMtr.moveRelative(-500, 200);
  rollerIntakeMtr.moveVelocity(200);
  profileController.setTarget("2ft", true);
  profileController.waitUntilSettled();

  //
  chassisTurn.turnAngle(-90*okapi::degree);
  profileController.setTarget("4ft", true);
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(-90*okapi::degree);
  profileController.setTarget("1ft", true);
  profileController.waitUntilSettled();
  profileController.setTarget("4ft", true);
  profileController.waitUntilSettled();
  profileController.setTarget("3ft", true);
  profileController.waitUntilSettled();

  /*
  chassisTurn.turnAngle(SCALED(-90));
  profileController.setTarget("4ft");
  profileController.waitUntilSettled();
  //chassisTurn.moveDistance(4.5*okapi::foot);
  rollerIntakeMtr.moveRelative(200, 200);
  profileController.setTarget("middleFlag");
  profileController.waitUntilSettled();
  //chassisTurn.moveDistance(2.5*okapi::foot);
  rollerIntakeMtr.moveVelocity(200);
  pros::delay(1000);

  //Part of auton in progress
  //Gets bottom flag, gets ball on cap,
  //Gets ball under other cap, shoots flags and gets bottom flag,
  //Gets onto platform
  flywheelMtr.moveVelocity(0);
  profileController.setTarget("bottomFlag");
  profileController.waitUntilSettled();
  capFlipperMtr.moveRelative(300, 200);
  pros::delay(100);
  capFlipperMtr.moveRelative(-300, 200);
  profileController.setTarget("bottomFlag", true);
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(90));
  rollerIntakeMtr.moveVelocity(200);
  profileController.setTarget("2ft");
  profileController.waitUntilSettled();
  capFlipperMtr.moveRelative(500, 200);
  profileController.setTarget("2ft", true);
  profileController.waitUntilSettled();
  rollerIntakeMtr.moveVelocity(0);

  chassisTurn.turnAngle(SCALED(90));
  profileController.setTarget("ball");
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(-90));
  rollerIntakeMtr.moveVelocity(200);
  capFlipperMtr.moveRelative(500, 200);
  profileController.setTarget("2ft");
  profileController.waitUntilSettled();
  rollerIntakeMtr.moveVelocity(0);
  capFlipperMtr.moveRelative(-500, 200);
  chassisTurn.turnAngle(SCALED(-90));
  flywheelMtr.moveVelocity(200);
  pros::delay(5000);
  rollerIntakeMtr.moveRelative(200, 200);
  profileController.setTarget("middleFlag");
  profileController.waitUntilSettled();
  rollerIntakeMtr.moveRelative(200, 200);
  profileController.setTarget("bottomFlag");
  profileController.waitUntilSettled();
  profileController.setTarget("alignWithPlatform", true);
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(90));
  profileController.setTarget("alignWithPlatform", true);
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(-90));
  profileController.setTarget("2ft", true);
  profileController.waitUntilSettled();
  chassisTurn.turnAngle(SCALED(-90));
  profileController.setTarget("2ft", true);
  profileController.waitUntilSettled();
  profileController.setTarget("4ft", true);
  profileController.waitUntilSettled();
  profileController.setTarget("2ft", true);
  profileController.waitUntilSettled();
  */
}

void auton1Blue() { auton1(false); }
void auton1Red() { auton1(true); }
void auton2Blue() { auton2(false); }
void auton2Red() { auton2(true); }
void auton3Blue() { auton3(false); }
void auton3Red() { auton3(true); }
void auton4Blue() { auton4(false); }
void auton4Red() { auton4(true); }

void autonsInit() {
  autons[0].description = "Blue square closest to flags";
  autons[0].action = auton1Blue;
  autons[1].description = "Red square closest to flags";
  autons[1].action = auton1Red;
  autons[2].description = "Blue square far";
  autons[2].action = auton2Blue;
  autons[3].description = "Red square far";
  autons[3].action = auton2Red;
  autons[4].description = "2nd Blue Front";
  autons[4].action = auton3Blue;
  autons[5].description = "2nd Red Front";
  autons[5].action = auton3Red;
  autons[6].description = "2nd Blue Back  ";
  autons[6].action = auton4Blue;
  autons[7].description = "2nd Red Back   ";
  autons[7].action = auton4Red;
  autons[8].description = "Skills auton";
  autons[8].action = autonSkill;
}

static int autonSelection;

int getNumAutons() {
  return numAutons;
}

int getAutonSelection() {
  return autonSelection;
}

void setAutonSelection(int option) {
  if (option >= numAutons) {
    autonSelection = numAutons - 1;
  } else if (option < 0) {
    autonSelection = 0;
  } else {
    autonSelection = option;
  }
}

std::string getAutonDescription() {
  return autons[autonSelection].description;
}

void startAuton() {
  autons[autonSelection].action();
}
