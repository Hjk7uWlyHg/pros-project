#include "main.h"

/**
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */
void opcontrol() {
	okapi::Controller masterController(okapi::ControllerId::master);
	okapi::Controller partnerController(okapi::ControllerId::partner);
	okapi::ControllerButton lock = masterController[okapi::ControllerDigital::X];
	okapi::ControllerButton track = masterController[okapi::ControllerDigital::Y];
	okapi::ControllerButton drive = masterController[okapi::ControllerDigital::B];
	okapi::ControllerButton color = masterController[okapi::ControllerDigital::A];
	auto chassis = okapi::ChassisControllerFactory::create(leftGroup, rightGroup);

	std::uint32_t colorTrack = 1;
	masterController.setText(0, 0,  colorTrack == 1 ? "Tracking: Blue " : "Tracking: Red  ");
	while (true) {
		if (color.changedToPressed()) {
			colorTrack = colorTrack % 2 + 1;
			masterController.setText(0, 0,  colorTrack == 1 ? "Tracking: Blue " : "Tracking: Red  ");
		}
		static std::uint8_t baseMode;
		if (drive.changedToPressed()) {
			baseMode = 0;
		} else if (lock.changedToPressed()) {
			baseMode = 1;
		} else if (track.changedToPressed()) {
			baseMode = 2;
		}
		switch (baseMode) {
			case 0:
				leftGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
				rightGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
				chassis.arcade(masterController.getAnalog(okapi::ControllerAnalog::leftY), masterController.getAnalog(okapi::ControllerAnalog::leftX));
				break;
			case 1:
				leftGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
				leftGroup.moveVelocity(0);
				rightGroup.setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
				rightGroup.moveVelocity(0);
				break;
			case 2:
				pros::vision_object_s_t object = vis.get_by_sig(0, colorTrack);
				std::cout << std::to_string(object.left_coord) << "\n";
				chassis.rotate((object.left_coord + object.width/2.0)/VISION_FOV_WIDTH);
				break;
		}
		rollerIntakeMtr.moveVelocity(200 * masterController.getAnalog(okapi::ControllerAnalog::rightY));
		flywheelMtr.moveVelocity(
			200 * masterController.getDigital(okapi::ControllerDigital::R1)
			- 200 * masterController.getDigital(okapi::ControllerDigital::R2)
		);
		static bool flywheelReady;
		if ((flywheelMtr.getActualVelocity() < -170.00) && !flywheelReady) {
			masterController.rumble("-");
			flywheelReady = true;
		} else if (flywheelMtr.getActualVelocity() >= -170.00) {
			flywheelReady = false;
		}
		if (partnerController.isConnected()) {
			descorerMtr.moveVelocity(-200 * partnerController.getAnalog(okapi::ControllerAnalog::leftY));
			capFlipperMtr.moveVelocity(-200 * partnerController.getAnalog(okapi::ControllerAnalog::rightY));
		} else {
			descorerMtr.moveVelocity(
				- 200 * masterController.getDigital(okapi::ControllerDigital::up)
				+ 200 * masterController.getDigital(okapi::ControllerDigital::down)
			);
			capFlipperMtr.moveVelocity(
				200 * masterController.getDigital(okapi::ControllerDigital::L1)
				- 200 * masterController.getDigital(okapi::ControllerDigital::L2)
			);
		}

		pros::Task::delay(10);
	}
}
