#pragma once

#include "main.h"

typedef struct {
  std::string description;
  void (*action)(void);
} Auton;

void autonsInit();
int getNumAutons();
int getAutonSelection();
void setAutonSelection(int);
std::string getAutonDescription();
void startAuton();
