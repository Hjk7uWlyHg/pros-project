#pragma once

#include "main.h"
#include "okapi/api.hpp"
#include "pros/vision.hpp"

#define CHASSIS_GEARSET okapi::AbstractMotor::gearset::green
#define WHEEL_DIAMETER (4.0 * okapi::inch)
#define CHASSIS_WIDTH (12.0 * okapi::inch)

#define DRIVE_GEARSET okapi::AbstractMotor::gearset::green
#define LEFT_MOTOR_1_PORT 3
#define LEFT_MOTOR_1_REVERSED false
#define LEFT_MOTOR_1_GEARSET okapi::AbstractMotor::gearset::green
#define LEFT_MOTOR_2_PORT 4
#define LEFT_MOTOR_2_REVERSED false
#define LEFT_MOTOR_2_GEARSET okapi::AbstractMotor::gearset::green
#define RIGHT_MOTOR_1_PORT 7
#define RIGHT_MOTOR_1_REVERSED true
#define RIGHT_MOTOR_1_GEARSET okapi::AbstractMotor::gearset::green
#define RIGHT_MOTOR_2_PORT 8
#define RIGHT_MOTOR_2_REVERSED true
#define RIGHT_MOTOR_2_GEARSET okapi::AbstractMotor::gearset::green

#define FLYWHEEL_MOTOR_PORT 9
#define FLYWHEEL_MOTOR_REVERSED true
#define FLYWHEEL_MOTOR_GEARSET okapi::AbstractMotor::gearset::green

#define ROLLER_INTAKE_MOTOR_PORT 6
#define ROLLER_INTAKE_MOTOR_REVERSED false
#define ROLLER_INTAKE_MOTOR_GEARSET okapi::AbstractMotor::gearset::green

#define DESCORER_MOTOR_PORT 5
#define DESCORER_MOTOR_REVERSED false
#define DESCORER_MOTOR_GEARSET okapi::AbstractMotor::gearset::green

#define CAP_FLIPPER_MOTOR_PORT 2
#define CAP_FLIPPER_MOTOR_REVERSED false
#define CAP_FLIPPER_MOTOR_GEARSET okapi::AbstractMotor::gearset::green

static okapi::Motor left1Mtr(LEFT_MOTOR_1_PORT, LEFT_MOTOR_1_REVERSED, LEFT_MOTOR_1_GEARSET);
static okapi::Motor left2Mtr(LEFT_MOTOR_2_PORT, LEFT_MOTOR_2_REVERSED, LEFT_MOTOR_2_GEARSET);
static okapi::Motor right1Mtr(RIGHT_MOTOR_1_PORT, RIGHT_MOTOR_1_REVERSED, RIGHT_MOTOR_1_GEARSET);
static okapi::Motor right2Mtr(RIGHT_MOTOR_2_PORT, RIGHT_MOTOR_2_REVERSED, RIGHT_MOTOR_2_GEARSET);
static okapi::Motor flywheelMtr(FLYWHEEL_MOTOR_PORT, FLYWHEEL_MOTOR_REVERSED, FLYWHEEL_MOTOR_GEARSET);
static okapi::Motor rollerIntakeMtr(ROLLER_INTAKE_MOTOR_PORT, ROLLER_INTAKE_MOTOR_REVERSED, ROLLER_INTAKE_MOTOR_GEARSET);
static okapi::Motor descorerMtr(DESCORER_MOTOR_PORT, DESCORER_MOTOR_REVERSED, DESCORER_MOTOR_GEARSET);
static okapi::Motor capFlipperMtr(CAP_FLIPPER_MOTOR_PORT, CAP_FLIPPER_MOTOR_REVERSED, CAP_FLIPPER_MOTOR_GEARSET);

static okapi::MotorGroup leftGroup({left1Mtr, left2Mtr});
static okapi::MotorGroup rightGroup({right1Mtr, right2Mtr});

static pros::Vision vis(11, pros::vision_zero_e_t::E_VISION_ZERO_CENTER);
static pros::vision_signature_s_t SIG_BLUE = {1, {1, 0, 0}, 3.0, -2979, -1567, -2272, 8865, 13825, 11346, 2173791, 0};
static pros::vision_signature_s_t SIG_RED = {2, {1, 0, 0}, 3.0, 8213, 11019, 9616, -1463, -219, -840, 8207157, 0};
